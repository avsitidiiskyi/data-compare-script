import requests
import json
import xml.etree.ElementTree as Etree
RED = '\033[0;31m'
GREEN = '\033[0;36m'
NORMAL = '\033[0m'


def extract_data_from_api():
    url = "https://jsonplaceholder.typicode.com/posts"
    with requests.get(url) as response:
        return response.text


tree = Etree.parse('posts.xml')

posts_xml = {}
for row in tree.getroot():
    _id_ = row.find("id").text
    title = row.find("title").text
    body = row.find("body").text
    posts_xml[_id_] = {'id': _id_, 'title': title, 'body': body}


matches = 0
does_not_match = 0
posts_api = json.loads(extract_data_from_api())
xml_ids = posts_xml.keys()

for post_api in posts_api:
    post_api_id = str(post_api["id"])
    if post_api_id in xml_ids:
        post_xml = posts_xml[post_api_id]
        if post_xml["title"] == post_api["title"] and post_xml["body"] == post_api["body"]:
            matches += 1
        else:
            print(f"Article №{post_api_id} does not match")
            does_not_match += 1

print(f"The quantity of matched elements is: {GREEN}{matches}")
print(f"{NORMAL}The quantity of mismatched elements is: {RED}{does_not_match}")
